import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
	
	x = np.linspace(-5, 5, 1000)
	y_tanh = np.tanh(x)
	y_softplus = np.log(1 + np.exp(x))
	y_relu = np.clip(x, 0, np.inf)
	y_leaky = np.clip(x, -np.inf, 0) * 0.1 + np.clip(x, 0, np.inf)
	y_softsign = x / (1 + np.abs(x))
	font = {'family': 'Times New Roman', 'weight': 'normal', 'size': 30}
	
	plt.figure(figsize=[5,4])
	
	plt.plot(x, y_tanh, label='Tanh', linewidth=3.0)
	plt.plot(x, y_softplus, label='SoftPlus', linewidth=3.0)
	plt.plot(x, y_relu, label='Relu', linewidth=3.0)
	plt.plot(x, y_leaky, label='LeakyRelu', linewidth=3.0)
	plt.plot(x, y_softsign, label='SoftSign', linewidth=3.0)
	
	# # tanh
	# plt.tick_params(labelsize=23)
	# plt.xlim(-5, 5)
	# plt.ylim(-1.5, 1.5)
	# my_x_ticks = np.arange(-5, 5.01, 1.0)
	# my_y_ticks = np.arange(-1.5, 1.51, 0.5)
	# plt.xticks(my_x_ticks)
	# plt.yticks(my_y_ticks)
	
	# # softsign
	# plt.tick_params(labelsize=23)
	# plt.xlim(-5, 5)
	# plt.ylim(-1.5, 1.5)
	# my_x_ticks = np.arange(-5, 5.01, 1.0)
	# my_y_ticks = np.arange(-1.5, 1.51, 0.5)
	# plt.xticks(my_x_ticks)
	# plt.yticks(my_y_ticks)
	
	# # softplus
	# plt.tick_params(labelsize=23)
	# plt.xlim(-5, 5)
	# plt.ylim(-1, 6.01)
	# my_x_ticks = np.arange(-5, 5.01, 1.0)
	# my_y_ticks = np.arange(-1, 6, 1)
	# plt.xticks(my_x_ticks)
	# plt.yticks(my_y_ticks)
	
	# # Relu
	# plt.tick_params(labelsize=23)
	# plt.xlim(-5, 5)
	# plt.ylim(-1, 6.01)
	# my_x_ticks = np.arange(-5, 5.01, 1.0)
	# my_y_ticks = np.arange(-1, 6, 1)
	# plt.xticks(my_x_ticks)
	# plt.yticks(my_y_ticks)
	
	# Relu
	plt.tick_params(labelsize=23)
	plt.xlim(-5, 5)
	plt.ylim(-1, 6.01)
	my_x_ticks = np.arange(-5, 5.01, 1.0)
	my_y_ticks = np.arange(-1, 6, 1)
	plt.xticks(my_x_ticks)
	plt.yticks(my_y_ticks)
	
	# plt.axis('off')
	plt.grid(linestyle='-.')
	plt.legend(prop=font)
	plt.show()