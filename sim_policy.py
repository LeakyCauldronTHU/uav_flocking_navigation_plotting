from Env_2D import EnvUAV
from ddpg import *
from plot_traj import *
import matplotlib.pyplot as plt
import time
import subprocess


# color = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 2
color = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray',
                 'tab:olive', 'tab:cyan']
font = {'weight': 'normal', 'size': 20}
color_traj = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 2

STEPS = 1000
NUM_AGENTS = 4
Sim_steps = 200
save_path = '../final_results/dual_' + str(NUM_AGENTS) + '/'
try:
    subprocess.run('mkdir ' + save_path, shell=True)
except:
    pass
# save_path = './maddpg_' + str(NUM_AGENTS) + '/'
env = EnvUAV(NUM_AGENTS)
# path = r'./data_transfer/pto_pre_action/uav_flocking_navigation_original_pto1_pre_action/saved_networks'
# path = r'./data_transfer/original/uav_flocking_navigation_original/saved_networks'
# path = r'./data_transfer/pre_action/uav_flocking_navigation_pred_action_3/saved_networks'
# path = r'./data_transfer/pre_action/uav_flocking_navigation_pred_action/saved_networks'
# path = r'./data_transfer/real/uav_flocking_navigation_real_other_3/saved_networks'
path = r'../data_transfer/master_dual/exp1/uav_flocking_navigation_real/saved_networks'
# path = r'./data_transfer/maddpg/exp1/uav_flocking_navigation_real/saved_networks'
# path = r'./data_transfer/original/'
# path = 'saved_networks'
agent = DDPG(env, path)

for i in range(0, Sim_steps):
    state = env.reset()
    if True:
        for step in range(STEPS):
            action = agent.test_action(state)
            # action = np.concatenate((np.random.uniform(-0.5, 1, [3, 1]), np.random.uniform(-1, 1, [3, 1])), 1)
            print('action', action)
            # File.write(str(action[0][1]))
            # File.write(' ')
            env.render()
            # action=np.array([[0,0.5],[0,0.5],[0,0.5]])
            
            state_next,reward,done,_ = env.forward(action)
            state = state_next
            print('reward', reward)
            if done.any():
                env.end()
                break
                
        draw_traj(color, color_traj, font, NUM_AGENTS, i, save_path)
        draw_speed(color, color_traj, font, NUM_AGENTS, i, save_path)
        draw_distance(color, color_traj, font, NUM_AGENTS, i, save_path)
        # time.sleep(5)
    # except:
    #     print("failed one sim")