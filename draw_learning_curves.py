import numpy as np
import matplotlib.pyplot as plt
import os

from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimSun'] # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题
mpl.rcParams['lines.markersize'] = 3
mpl.rcParams["legend.markerscale"] = 6.0
mpl.rcParams["legend.framealpha"] = 1.0
font = {'weight': 'normal', 'size': 20}


if __name__ == "__main__":
    LEN = 250
    experiments = ['./data_transfer/maddpg/', './data_transfer/maddpg_dual/', './data_transfer/pto_pre_action/',
                   './data_transfer/pot1/', './data_transfer/original/']
    exp_index = 0
    sub_dirs = os.listdir(experiments[exp_index])
    files = [experiments[exp_index] + sub + '/uav_flocking_navigation_real/Result/reward.txt' for sub in sub_dirs]
    plt.figure(figsize=(9, 8))
    average_reward = []
    for i in range(len(files)):
        rewards = np.loadtxt(files[i])
        average_reward.append(rewards[0:LEN, 1])
        # print("shape", np.shape(rewards[0:LEN, 1]))
        plt.plot(rewards[0:LEN, 0], rewards[0:LEN, 1], label=str(i))
    average_reward = sum(average_reward) / len(average_reward)
    # print("check", np.shape(average_reward))
    plt.plot(rewards[0:LEN, 0], average_reward, label='average', linewidth=4)
    plt.grid(linestyle='-.')
    plt.tick_params(labelsize=20)
    plt.xlim(0, 50000)
    plt.xlabel('训练步数', fontsize=20)
    plt.ylabel('期望累积回报', fontsize=20)
    plt.legend(loc=2, prop=font)
    plt.show()
