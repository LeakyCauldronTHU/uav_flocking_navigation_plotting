# -----------------------------------
# Deep Deterministic Policy Gradient
# Author: Flood Sung
# Date: 2016.5.4
# -----------------------------------
# import tensorflow as tf
try:
        import tensorflow.compat.v1 as tf
        tf.disable_v2_behavior()
except:
        import tensorflow as tf

import numpy as np
import random
from collections import deque
import networks


class Normalizer(object):
    def __init__(self):
        self.mean = -4
        self.std = 20
        self.decay_rate = 0.999

    def normalize(self, reward):
        self.mean = self.decay_rate * self.mean + (1. - self.decay_rate) * reward.mean()
        self.std = self.decay_rate * self.std + (1. - self.decay_rate) * reward.std()
        return self.mean, self.std


# Hyper Parameters:
REPLAY_BUFFER_SIZE = 30000
REPLAY_START_SIZE = 5000
BATCH_SIZE = 8
GAMMA = 0.99
LAYER1_SIZE = 400
LAYER2_SIZE = 300
Q_LEARNING_RATE = 0.001
P_LEARNING_RATE = 0.0001
TAU = 0.001
L2_Q = 0.01
L2_POLICY = 0.0
MAX_GRADNORM = 10.0
PURE_STATE_DIM = 21


def state_filter(state):
        return state[:, 0:PURE_STATE_DIM]


class DDPG:
        def __init__(self, env, path):
                self.path = path
                self.environment = env
                self.reward_normalizer = Normalizer()
                
                state_dim = env.num_states
                action_dim = env.num_actions
                # Initialize time step
                self.time_step = 0
                # initialize replay buffer
                self.replay_buffer = deque()
                # initialize networks
                self.create_networks_and_training_method(state_dim,action_dim)

                self.sess = tf.InteractiveSession()
                with tf.name_scope('initializing_global_variables'):
                        self.sess.run(tf.global_variables_initializer())
                        
                with tf.name_scope('initializing_target_networks'):
                        self.sess.run(self.init_p)
                        self.sess.run(self.init_q)

                # loading networks
                with tf.name_scope('saving_and_loading_networks'):
                        self.saver = tf.train.Saver()
                        checkpoint = tf.train.get_checkpoint_state(self.path)
                        if checkpoint and checkpoint.model_checkpoint_path:
                                        self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
                                        print("Successfully loaded:", checkpoint.model_checkpoint_path)
                        else:
                                        print("Could not find old network weights")

                        global summary_writer
                        summary_writer = tf.summary.FileWriter('logs',graph=self.sess.graph)
        
        def create_networks_and_training_method(self,state_dim,action_dim):

                with tf.name_scope('defining_variables'):
                        theta_p = networks.theta_p(PURE_STATE_DIM,action_dim)
                        theta_q = networks.theta_q(state_dim,action_dim)
                        target_theta_p = networks.theta_p(PURE_STATE_DIM,action_dim)
                        target_theta_q = networks.theta_q(state_dim,action_dim)
                        
                with tf.name_scope('initialize_target_networks'):
                        init_target_p = []
                        for i in range(len(theta_p)): init_target_p.append(target_theta_p[i].assign(theta_p[i]))
                        init_target_q = []
                        for i in range(len(theta_q)): init_target_q.append(target_theta_q[i].assign(theta_q[i]))
                        self.init_p = tf.group(init_target_p[0],init_target_p[1],
                                               init_target_p[2],init_target_p[3],
                                               init_target_p[4],init_target_p[5])
                        self.init_q = tf.group(init_target_q[0],init_target_q[1],
                                               init_target_q[2],init_target_q[3],
                                               init_target_q[4],init_target_q[5])
                        target_update_p = []
                        for i in range(len(theta_p)): target_update_p.append(target_theta_p[i].assign(TAU*theta_p[i]+(1-TAU)*target_theta_p[i]))
                        target_update_q = []
                        for i in range(len(theta_q)): target_update_q.append(target_theta_q[i].assign(TAU*theta_q[i]+(1-TAU)*target_theta_q[i]))
                        
                with tf.name_scope('placeholder_variables'):
                        self.state = tf.placeholder(tf.float32,[None,state_dim],'state')
                        self.action = tf.placeholder(tf.float32,[None,action_dim],'action')
                        self.reward = tf.placeholder(tf.float32,[None],'reward')
                        self.next_state = tf.placeholder(tf.float32,[None,state_dim],'next_state')
                        self.done = tf.placeholder(tf.bool,[None],'done')
                        self.reward_mean = tf.placeholder(tf.float32,[None],'reward_mean')
                        self.reward_std = tf.placeholder(tf.float32,[None],'reward_std')

                with tf.name_scope('behavior_action'):
                        self.action_behavior = networks.policy_network(state_filter(self.state),theta_p,'actor')

                with tf.name_scope('policy_optimization'):                                
                        with tf.name_scope('critic_for_actor_training'):
                                action_train = networks.policy_network(state_filter(self.state),theta_p,'actor')
                                q_train = networks.q_network(self.state, action_train, theta_q,'critic')

                        with tf.name_scope('optimizing_actor'):
                                optim_p = tf.train.AdamOptimizer(P_LEARNING_RATE)
                                weight_decay_p = tf.add_n([L2_POLICY * tf.nn.l2_loss(var) for var in theta_p])
                                loss_p = tf.reduce_mean(-q_train) + weight_decay_p
                                grads_and_vars_p,var_p = zip(*optim_p.compute_gradients(loss_p,var_list=theta_p)) 
                                grads_and_vars_p,_ = tf.clip_by_global_norm(grads_and_vars_p,MAX_GRADNORM)
                                self.grads_and_vars_p = grads_and_vars_p
                                optimize_p = optim_p.apply_gradients(zip(grads_and_vars_p,var_p))
                                
                with tf.name_scope('updating_actor_network'):
                        with tf.control_dependencies([optimize_p]):
                                self.train_p = tf.group(target_update_p[0],target_update_p[1],
                                                        target_update_p[2],target_update_p[3],
                                                        target_update_p[4],target_update_p[5])
                        
                with tf.name_scope('critic_optimization'):
                        with tf.name_scope('next_action_and_next_q'):
                                present_q = networks.q_network(self.state,self.action,theta_q,'critic')
                                action_next = networks.policy_network(state_filter(self.next_state),target_theta_p,'target_actor')
                                next_q = networks.q_network(self.next_state,action_next,target_theta_q,'target_critic')
                                
                        with tf.name_scope('critic_optimizing'):
                                q_target = tf.stop_gradient(tf.where(self.done,self.reward,self.reward + GAMMA * next_q))
                                q_error = tf.reduce_mean(tf.square(q_target - present_q))
                                weight_decay_q = tf.add_n([L2_Q * tf.nn.l2_loss(var) for var in theta_q])
                                loss_q = q_error + weight_decay_q
                                optim_q = tf.train.AdamOptimizer(Q_LEARNING_RATE)
                                grads_and_vars_q,var_q = zip(*optim_q.compute_gradients(loss_q, var_list=theta_q))
                                grads_and_vars_q,_ = tf.clip_by_global_norm(grads_and_vars_q,MAX_GRADNORM)
                                optimize_q = optim_q.apply_gradients(zip(grads_and_vars_q,var_q))

                with tf.name_scope('updating_critic_network'):
                        with tf.control_dependencies([optimize_q]):
                                self.train_q = tf.group(target_update_q[0],target_update_q[1],
                                                        target_update_q[2],target_update_q[3],
                                                        target_update_q[4],target_update_q[5])

                with tf.name_scope('summaries'):
                        tf.summary.scalar("loss_q",loss_q)
                        tf.summary.scalar("loss_p",loss_p)
                        tf.summary.scalar("reward_mean", tf.reduce_sum(self.reward_mean))
                        tf.summary.scalar("reward_std", tf.reduce_sum(self.reward_std))
                        
                        global merged_summary_op
                        merged_summary_op = tf.summary.merge_all()

        def train(self, reward_mean, reward_std):
                minibatch = random.sample(self.replay_buffer,BATCH_SIZE)
                state_batch = [data[0] for data in minibatch]
                action_batch = [data[1] for data in minibatch]
                reward_batch = [data[2] for data in minibatch]
                next_state_batch = [data[3] for data in minibatch]
                done_batch = [data[4] for data in minibatch]
                
                _,_,summary_str = self.sess.run([self.train_p,self.train_q,merged_summary_op],feed_dict={
                        self.state:state_batch,
                        self.action:action_batch,
                        self.reward:reward_batch,
                        self.next_state:next_state_batch,
                        self.done:done_batch,
                        self.reward_mean:[reward_mean],
                        self.reward_std:[reward_std],
                        })
                summary_writer.add_summary(summary_str,self.time_step-REPLAY_START_SIZE)

                # save network every 1000 iteration
                if self.time_step % 1000 == 0:
                        self.saver.save(self.sess, 'saved_networks/' + 'network', global_step = self.time_step-REPLAY_START_SIZE)

        def behavior_action(self,state):
                action = self.sess.run(self.action_behavior,feed_dict={
                        self.state:state
                        })
                # noise = np.random.uniform(-0.25,0.25,[2])
                noise = np.random.normal(0,0.2,[2])
                # print('noiseless action',action)
                action = action + noise
                return np.clip(action,self.environment.action_space_low,self.environment.action_space_high)

        def test_action(self,state):
                action = self.sess.run(self.action_behavior,feed_dict={
                        self.state:state
                        })
                return np.clip(action,self.environment.action_space_low,self.environment.action_space_high)
        
        def perceive(self,state,action,reward,next_state,done):
                self.replay_buffer.append((state,action,reward,next_state,done))
                # Update time step
                self.time_step += 1

                # Limit the replay buffer size
                if len(self.replay_buffer) > REPLAY_BUFFER_SIZE:
                        self.replay_buffer.popleft()

                # Store transitions to replay start size then start training
                if self.time_step > REPLAY_START_SIZE:
                        # print('is training step', self.time_step - REPLAY_START_SIZE)
                        reward_mean, reward_std = self.reward_normalizer.normalize(reward)
                        self.train(reward_mean, reward_std)
