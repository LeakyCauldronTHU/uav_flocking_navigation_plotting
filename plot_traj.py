import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimSun'] # 指定默认字体
mpl.rcParams['axes.unicode_minus'] = False # 解决保存图像是负号'-'显示为方块的问题
mpl.rcParams['lines.markersize'] = 1.5
mpl.rcParams["legend.markerscale"] = 6.0
mpl.rcParams["legend.framealpha"] = 1.0


def draw_traj(color, color_traj, font, num_agents, num, path=''):
    mat_height = np.loadtxt('./Result/mat_height.txt')
    mat_exist = np.loadtxt('./Result/mat_exist.txt')
    # 绘制地图
    # fig = plt.figure(figsize=(5, 5))
    plt.figure(figsize=(10, 10))
    
    M, N = np.shape(mat_height)
    for i in range(0, 10):
        for j in range(0, 10):
            if mat_exist[i + 8, j + 8] > 0:
                centroid = [i * 200 + 100, j * 200 + 100]
                color_index = int((mat_height[i + 8, j + 8] - 30) // 17)
                circle = plt.Circle((centroid[0], centroid[1]), 60, color=color[color_index], fill=True)
                plt.gcf().gca().add_artist(circle)
    
    # plt.axis('equal')
    plt.xlim(0, 2000)
    plt.ylim(0, 2000)
    plt.axis('off')
    
    # 绘制轨迹
    for n in range(num_agents):
        traj = np.loadtxt('./Result/path_agent' + str(n) + '.txt')
        traj_length = np.shape(traj)[0]
        plt.scatter(traj[0, 2], traj[0, 3], color=color_traj[n], linewidths=10)
        for i in range(1, traj_length):
            plt.scatter(traj[i, 2], traj[i, 3], color=color_traj[n], linewidths=1)
        
        plt.scatter(traj[i, 2], traj[i, 3], color=color_traj[n], linewidths=1, label='无人机' + str(n))
    
    target = traj[0, 0:2]
    plt.scatter(target[0], target[1], color=color[-1], linewidths=10)
    
    plt.legend(loc=2, prop=font)
    plt.savefig(path + str(num) + '_traj.png', dpi=300)
    # plt.show()
    # exit(0)


def draw_speed(color, color_traj, font, num_agents, num, path=''):
    plt.figure(figsize=(9, 8))
    for n in range(num_agents):
        speed = np.loadtxt('./Result/speed_agent' + str(n) + '.txt')
        plt.plot([i for i in range(len(speed))], speed, label='无人机' + str(n), linewidth=2.0)

    # plt.legend(loc=2, prop=font)
    plt.tick_params(labelsize=20)
    plt.ylim([0, 10])
    plt.xlabel('时间（秒）', fontsize=20)
    plt.ylabel('速度（米/秒）', fontsize=20)
    plt.grid(linestyle='-.')
    plt.legend(loc=2, prop=font)
    plt.savefig(path + str(num) + '_speed.png', dpi=150)
    # plt.show()
    

def draw_distance(color, color_traj, font, num_agents, num, path=''):
    plt.figure(figsize=(12, 11))
    for n in range(num_agents):
        distance = np.loadtxt('./Result/distance_agent' + str(n) + '.txt')
        plt.plot([i for i in range(len(distance[:,0]))], distance[:, 0], label='无人机' + str(n) + ' 左侧', linewidth=3)
        plt.plot([i for i in range(len(distance[:,1]))], distance[:, 1], label='无人机' + str(n) + ' 右侧', linewidth=3)

    plt.tick_params(labelsize=30)
    plt.xlabel('时间（秒）', fontsize=30)
    plt.ylabel('距离（米）', fontsize=30)
    plt.legend(loc=2, prop=font)
    plt.grid(linestyle='-.')
    plt.savefig(path + str(num) + '_distance.png', dpi=150)
    # plt.show()


def draw_action():
    x = np.linspace(0, 10, 100)
    y = -np.tanh(0.5 * (x - 10.0))
    plt.plot(x, y)
    # plt.savefig('books_read.png')
    plt.show()
    

if __name__ == "__main__":
    
    num_agents = 7
    # color = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 2
    color = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    font = {'weight': 'normal', 'size': 20}
    color_traj = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 2

    draw_traj(color, color_traj, font, num_agents, 0)

    draw_speed(color, color_traj, font, num_agents, 0)
    draw_distance(color, color_traj, font, num_agents, 0)
    
    # draw_action()
