from Env_2D import EnvUAV
from ddpg import *

EPISODES = 100000
STEPS = 500
TEST = 200
REPLAY_START_SIZE = 10000
NUM_AGENTS = 3
def main():
    env = EnvUAV(NUM_AGENTS)
    agent = DDPG(env)
    Flag = False
    start_training = 0
    counter = 0
    for episode in range(EPISODES):
        state = env.reset()
        # Train
        print('agents start collecting episode '+str(episode))
        for step in range(STEPS):
            counter = counter + 1
            action = agent.behavior_action(state)
            next_state,reward,done,_ = env.forward(action)
            #env.render()
            for i in range(NUM_AGENTS):
                agent.perceive(state[i],action[i],reward[i],next_state[i],done[i])
                #print('state',state)
                #print('action',action)
                #print('reward',reward)
                #print('next_state',next_state)
                #print('done',done)

                #print('distance to the closest agent:',np.log(2/(state[i,-1]+1)-1)/(-0.02))

            if done.any():
                #print('done',done)
                break
            state = next_state
            
        if (counter > REPLAY_START_SIZE) and (not start_training):
            start_training = episode
            Flag = True

        '''
         # Testing:
        File_reward = open('./Result/reward.txt','a')
        if (episode-start_training) % 500 == 0 and Flag:
            total_reward = 0
            for i in range(TEST):
                state = env.reset()
                for j in range(100):
                    #env.render()
                    action = agent.test_action(state) # direct action for test
                    state,reward,done,_ = env.forward(action[0])
                    #print('reward',reward)
                    total_reward += reward
                    if done:
                        break
            ave_reward = total_reward/TEST
            File_reward.write(str(ave_reward))
            File_reward.write(' ')
            File_reward.write(str(episode-start_training))
            File_reward.write('\n')
            print('episode: ',episode,'Evaluation Average Reward:',ave_reward)
        File_reward.close()
        '''
        
if __name__ == '__main__':
    main()





    
